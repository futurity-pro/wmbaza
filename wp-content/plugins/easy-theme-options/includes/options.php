<?php

/* ----------------------------------------
* To retrieve a value use: $eto_options[$prefix.'var']
----------------------------------------- */

$prefix = 'eto_';

/* ----------------------------------------
* Create the TABS
----------------------------------------- */

$eto_custom_tabs = array(
		array(
			'label'=> __('Настройки', 'eto'),
			'id'	=> $prefix.'general'
		)
	);

/* ----------------------------------------
* Options Field Array
----------------------------------------- */

$eto_custom_meta_fields = array(

	/* -- TAB 1 -- */
	array(
		'id'	=> $prefix.'general', // Use data in $eto_custom_tabs
		'type'	=> 'tab_start'
	),
	array(
		'label'=> 'Адрес',
		'desc'	=> 'Адрес в верхушке сайта',
		'id'	=> $prefix.'address',
		'type'	=> 'textarea'
	),
    array(
        'label'=> 'Disclaimer',
        'desc'	=> 'Текст в подвале сайта',
        'id'	=> $prefix.'disclaimer',
        'type'	=> 'textarea'
    ),
    array(
		'label'=> 'Email',
		'desc'	=> '',
		'id'	=> $prefix.'email',
		'type'	=> 'text'
	),
    array(
        'label'=> 'Skype',
        'desc'	=> '',
        'id'	=> $prefix.'skype',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'Телефон',
        'desc'	=> '',
        'id'	=> $prefix.'phone',
        'type'	=> 'text'
    ),

    array(
        'label'=> 'WMID',
        'desc'	=> 'Номер счета в WebMoney',
        'id'	=> $prefix.'wmid',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'WMZ',
        'desc'	=> 'Номер кошелька WMZ',
        'id'	=> $prefix.'wmz',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'WMU',
        'desc'	=> 'Номер кошелька WMU',
        'id'	=> $prefix.'wmu',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'WME',
        'desc'	=> 'Номер кошелька WME',
        'id'	=> $prefix.'wme',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'WMR',
        'desc'	=> 'Номер кошелька WMR',
        'id'	=> $prefix.'wmr',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'WMZ в UAH',
        'desc'	=> 'Курс WMZ в UAH',
        'id'	=> $prefix.'wmz-uah',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'WMU в UAH',
        'desc'	=> 'Курс WMU в UAH',
        'id'	=> $prefix.'wmu-uah',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'WME в UAH',
        'desc'	=> 'Курс WME в UAH',
        'id'	=> $prefix.'wme-uah',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'WMR в UAH',
        'desc'	=> 'Курс WMR в UAH',
        'id'	=> $prefix.'wmr-uah',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'Комиссия, %',
        'desc'	=> 'Комиссия обменки',
        'id'	=> $prefix.'fee',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'Комиссия WMT, %',
        'desc'	=> 'Комиссия WebMoney Transfer',
        'id'	=> $prefix.'wmtfee',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'Минимальная сумма вывода',
        'desc'	=> '',
        'id'	=> $prefix.'min',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'Максимальная сумма вывода',
        'desc'	=> '',
        'id'	=> $prefix.'max',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'Сумма по умолчанию',
        'desc'	=> '',
        'id'	=> $prefix.'from',
        'type'	=> 'text'
    ),
    array(
        'label'=> 'Задержка при обновлении, мс',
        'desc'	=> 'Задержка нужна, чтобы клиент успел ввести сумму, перед тем, как поле ввода обновится',
        'id'	=> $prefix.'delay',
        'type'	=> 'text'
    ),
	array(
		'type'	=> 'tab_end'
	),
	/* -- /TAB 1 -- */
);

?>
<?php get_header() ?>
<section id="content" class="content step-1">
    <form class="form form-calculator">
        <div class="input-line">
            <div id="select" class="from"></div>
            <div id="select-list" class="from-select">
                <ul>
                    <li><a href="#">WMZ</a></li>
                    <li><a href="#">WMU</a></li>
                    <li><a href="#">WME</a></li>
                    <li><a href="#">WMR</a></li>
                </ul>
            </div>
            <div class="from-input">
                <label for="from">У Вас есть:<br>
                    <span id="fromSymbol"></span>
                    <input id="from" maxlength="6">
                </label>
            </div>
            <div class="center"></div>
            <div class="to-input">
                <label for="to">Вы получаете:<br>
                    <span id="toSymbol"></span>
                    <input id="to" maxlength="6">
                </label>
            </div>
            <div class="to"></div>
            <br class="clear">
        </div>
        <div class="notices">
            <div class="fee">Комиссия: <span id="fee"></span>%</div>
            <div class="error">
                <div class="error-min active">
                    Слишком маленькая сумма вывода<br>
                    (разрешено от <span id="min"></span> ₴)
                </div>
                <div class="error-max active">
                    Недостаточно валюты (есть <span id="max"></span> ₴)
                </div>
            </div>
            <div class="currency">грн. Наличными</div>
            <br class="clear">
        </div>
        <div id="price" class="price">
            1.00 WMZ - 7.85 UAH
        </div>
        <div class="actions">
            <a id="submit" href="#" class="btn">Обменять <i class="icon next"></i></a>
        </div>
        <div class="form-order">
            <div class="info">
                Уважаемый посетитель!<br>
                Вывод титульных знаков WM осуществляется через заполнение формы заявки
                и последующей оплаты Вами сделки наличными в офисе.
            </div>
            <h2>ЗАЯВКА НА ОБМЕН</h2>
            <div class="main-fields">
                <div class="column">
                    <label>Ваш WMID: *</label>
                    <input id="your-wmid" class="required" type="text" pattern="[0-9.]+" maxlength="12">
                    <span id="your-wmid-required" class="error">Требуется указать WMID</span>
                </div>
                <div class="column">
                    <label>Серия и номер паспорта: *</label>
                    <input id="your-passport" class="required">
                    <span id="your-passport-required" class="error">Требуется заполнить поле Серия и № паспорта.</span>
                </div>
                <br class="clear">
            </div>
            <div class="additional-fields">
                <h4>Дополнительные поля</h4>
                <div class="field">
                    <label>Имя: *</label>
                    <input id="your-name" class="required" onkeypress="return filter_input(event,/[A-ZА-Я]/i)">
                    <span id="your-name-required" class="error">Укажите, пожалуйста, своё имя.</span>
                </div>
                <div class="field">
                    <label>Фамилия: *</label>
                    <input id="your-surname" class="required" onkeypress="return filter_input(event,/[A-ZА-Я]/i)">
                    <span id="your-surname-required" class="error">Пожалуйста, укажите свою фамилию.</span>
                </div>
                <div class="field">
                    <label>Номер карты: *</label>
                    <input id="cardnumber" class="required">
                    <span id="cardnumber-required" class="error">Пожалуйста, укажите номер карты.</span>
                </div>
                <div class="field">
                    <label>Телефон:</label>
                    <input id="your-phone">
                    <span id="your-phone-required" class="error">Заполните телефон</span>
                </div>
                <div class="field">
                    <label>E-mail: *</label>
                    <input id="your-email" class="required">
                    <span id="your-email-required" class="error">Пожалуйста, укажите свой адрес электронной почты.</span>
                    <span id="your-email-wrong" class="error">Пожалуйста, укажите корректный адрес электронной почты.</span>
                </div>
            </div>
            <div class="fee-fields">
                <div class="column">
                    <div class="field">
                        <label>Комиссия WMT:</label>
                        <input class="wmtfee" disabled="disabled">
                    </div>
                    <div class="field">
                        <label>Необходимо иметь:</label>
                        <input class="have" disabled="disabled">
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label>Комиссия обмена:</label>
                        <input class="totalfee" disabled="disabled">
                    </div>
                    <div class="field">
                        <label>Вы получаете:</label>
                        <input class="result" disabled="disabled">
                    </div>
                </div>
                <br class="clear">
            </div>
            <div class="description">
                * обязательные поля
            </div>
            <div class="rules-fields">
                <input type="checkbox" id="rules"> <label for="rules">С <a target="_blank" href="/soglashenie">правилами</a> ознакомлен</label><br>
                <span id="rules-required" class="error">Вы должны принять условия пользовательского соглашения.</span><br>
                <input type="checkbox" id="remember"> <label for="remember"> Запомнить данные</label>
            </div>
            <div class="actions">
                <a id="order" href="#" class="btn">Подать заявку <i class="icon next"></i></a>
            </div>
        </div>
    </form>
    <form class="form form-confirmation" id="user-data" action="<?php echo get_template_directory_uri(); ?>/order.php">
        <div class="form-order">
            <h2>ПОДТВЕРЖДЕНИЕ ЗАЯВКИ</h2>
            <div class="main-fields">
                <div class="column">
                    <label>Ваш WMID: *</label>
                    <input id="your-wmid-confirm" name="wmid" disabled="disabled">
                </div>
                <div class="column">
                    <label>Серия и номер паспорта: *</label>
                    <input id="your-passport-confirm" name="passport" disabled="disabled">
                </div>
                <br class="clear">
            </div>
            <div class="additional-fields">
                <h4>Дополнительные поля</h4>
                <div class="field">
                    <label>Имя: *</label>
                    <input id="your-name-confirm"  name="name" disabled="disabled">
                </div>
                <div class="field">
                    <label>Фамилия: *</label>
                    <input id="your-surname-confirm"  name="surname" disabled="disabled">
                </div>
                <div class="field">
                    <label>Номер карты: *</label>
                    <input id="cardnumber-confirm"  name="cardnumber" disabled="disabled">
                </div>
                <div class="field">
                    <label>Телефон:</label>
                    <input id="your-phone-confirm"  name="phone" disabled="disabled">
                </div>
                <div class="field">
                    <label>E-mail: *</label>
                    <input id="your-email-confirm"  name="email" disabled="disabled">
                </div>
            </div>
            <div class="fee-fields">
                <div class="column">
                    <div class="field">
                        <label>Комиссия WMT:</label>
                        <input class="wmtfee"  name="wmtfee" disabled="disabled">
                    </div>
                    <div class="field">
                        <label>Необходимо иметь:</label>
                        <input class="have"  name="have" disabled="disabled">
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label>Комиссия обмена:</label>
                        <input class="totalfee"  name="totalfee" disabled="disabled">
                    </div>
                    <div class="field">
                        <label>Вы получаете:</label>
                        <input class="result"  name="result" disabled="disabled">
                    </div>
                </div>
                <br class="clear">
            </div>
        </div>
        <div class="actions">
            <a id="cancel" href="#" class="btn-red">Отказаться от сделки</a>
            <a id="confirm" href="#" class="btn btn-big">Подтвердить сделку <i class="icon next"></i></a>
        </div>
    </form>
    <form class="form form-thanks">
        <h2>Заявка отправлена</h2>
        <div class="info">
            <p>Заявка успешно отправлена.<br>
                На Ваш e-mail отправлено письмо с дальнейшей инструкцией.</p>
            <p><a class="init" href="#">Кликните сюда, если Вы не хотите больше ждать</a></p>
        </div>
    </form>
    <form class="form form-cancel">
        <h2>Заявка отменена</h2>
        <div class="info">
            <p>Вы отменили заявку. Движения по ней теперь невозможно.<br>
                Если Вы хотите всё же совершить обмен, пожалуйста, оставьте новую заявку.</p>
            <p><a class="init" href="#">Кликните сюда, если Вы не хотите больше ждать</a></p>
        </div>
    </form>
    <?php while ( have_posts() ) : the_post(); ?>
    <article>
        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
    </article>
    <?php endwhile; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer() ?>
function filter_input(e,regexp)
{
    e=e || window.event;
    var target=e.target || e.srcElement;
    var isIE=document.all;

    if (target.tagName.toUpperCase()=='INPUT')
    {
        var code=isIE ? e.keyCode : e.which;
        if (code<32 || e.ctrlKey || e.altKey) return true;

        var char=String.fromCharCode(code);
        if (!regexp.test(char)) return false;
    }
    return true;
}

jQuery(function($){
    var initTimer;
    var calculatorEnabled = true;

    function scrollToTop() {
        document.location.href = '#content';
    }

    function init() {
        if(initTimer) {
            clearTimeout(initTimer);
        }

        $('#from').val(options.from).select();

        $('.form, .form-order').hide();
        $('.form-calculator').show();
        $('#content').removeClass('step-2').removeClass('step-3').addClass('step-1');
        $('#min').text(options.min);
        $('#max').text(options.max);
        $('#fee').text(options.fee);

        $('.form-order input').val('');

        $('.required').each(function(){
            var currentVal = $.cookie($(this).attr('id'));
            $('#'+$(this).attr('id')).val(currentVal);
        });

        $('#your-phone').val($.cookie('your-phone'));

        calculatorEnabled = true;
        calculate(false);

        return false;
    }

    function initStart() {
        initTimer = setTimeout(init, 10000);
    }

    init();

    $('#submit').click(function() {
        if(!$(this).attr('disabled')) {
            calculatorEnabled = false;
            calculate(false);

            $('.form-order').show();
            $('#content').removeClass('step-1').addClass('step-2');
        }

        return false;
    });

    $('#order').click(function(){
        var valid = true;

        $('.error').removeClass('active');

        $('.required').each(function(){
            var currentVal = $(this).val();
            if(!currentVal) {
                $('#'+$(this).attr('id')+'-required').addClass('active');
                valid = false;
            }
        })

        if($('#your-email').val().length>0 && $('#your-email').val().indexOf('@')==-1) {
            valid = false;
            $('#your-email-wrong').addClass('active');
        }

        if(!$('#rules').prop('checked')) {
            $('#rules-required').addClass('active');
            valid = false;
        }

        if(valid) {
            scrollToTop();

            $('.required').each(function(){
                var currentVal = $(this).val();
                $('#'+$(this).attr('id')+'-confirm').val(currentVal);
                if($('#remember').prop('checked')) {
                    $.cookie($(this).attr('id'), currentVal);
                }
            });

            $('#your-phone-confirm').val($('#your-phone').val());
            if($('#remember').prop('checked')) {
                $.cookie('your-phone', $('#your-phone').val());
            }

            $('.form').hide();
            $('.form-confirmation').show();
        }

        return false;
    });

    $('#confirm').click(function(){
        scrollToTop();

        $('#user-data input').prop('disabled', false);
        var userData = $('#user-data').serialize();
        $.post($('#user-data').attr('action'), userData);
        $('#user-data input').prop('disabled', true);

        $('.form').hide();
        $('.form-thanks').show();
        $('#content').removeClass('step-2').addClass('step-3');

        initStart();

        return false;
    });

    $('#cancel').click(function(){
        scrollToTop();

        $('.form').hide();
        $('.form-cancel').show();

        initStart();

        return false;
    });

    $('.init').click(function(){
        init();
        scrollToTop();
    });

    $('.faq h3').click(function(){
        $(this).parent().toggleClass('active');
    });

    $('#news').click(function(){
        $('.page').fadeOut();
        $('#newscontent').fadeIn();
    });

    $('#from,#to').keydown(function(){
        return ( event.ctrlKey || event.altKey
            || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false)
            || (95<event.keyCode && event.keyCode<106)
            || (event.keyCode==8) || (event.keyCode==9)
            || (event.keyCode>34 && event.keyCode<40)
            || (event.keyCode==46)
            || ((event.keyCode==190 || event.keyCode==110) && $(this).val().indexOf('.')==-1)
            );
    });

    function calculate(reverse) {
        if(calculatorEnabled) {
            $('#to,#from').prop('disabled', false);
        } else {
            $('#to,#from').prop('disabled', true);
        }

        $('#select').text(options.current.toUpperCase());
        $('#price').text('1 '+options.current.toUpperCase()+' - '+options[options.current+'_uah']+' UAH');

        if(reverse) {
            var result = options.to + (options.to * options.fee/100);
            // 72000 + 2880 = 74880
            options.from = result / options[options.current+'_uah'];
            // 74880 / 7.5 = 9984

        } else {
            var result = options.from * options[options.current+'_uah'];
            // 10000 * 7.5 = 75000
            options.to = result - (result * options.fee/100);
            // 75000 - 3000 = 72000
        }

        var isError = false;

        if(options.to < options.min) {
            $('.error-min').addClass('active');
            isError = true;
        } else {
            $('.error-min').removeClass('active');
        }

        if(options.to > options.max) {
            $('.error-max').addClass('active');
            isError = true;
        } else {
            $('.error-max').removeClass('active');
        }

        if (isError) {
            $('#submit').attr('disabled', 'disabled');
        } else {
            $('#submit').removeAttr('disabled');
        }

        var wmtfee = options.wmtfee/100 * options.from;
        $('.wmtfee').val(wmtfee);
        $('.have').val(wmtfee + options.from);
        $('.totalfee').val(result - options.to);
        $('.result').val(options.to);

        if(reverse) {
            $('#from').val(options.from);
        } else {
            $('#to').val(options.to);
        }

        $('#fromSymbol').text(options[options.current]);
        $('#toSymbol').text(options['wmu']);
    }

    $('#select').click(function(){
        if (calculatorEnabled) {
            if ($('#select').hasClass('active')) {
                $('#select-list').hide();
                $('#select').removeClass('active');
            } else {
                $('#select').addClass('active');
                $('#select-list').show();
            }
        }
        return false;
    });

    $('#select-list a').click(function(){
        options.current = $(this).text().toLowerCase();
        $('#select-list').hide();
        calculate(false);
        $('#select').removeClass('active');

        return false;
    });

    $('body').click(function(){
        $('#select-list').hide();
        $('#select').removeClass('active');
    });

    $('#from').keyup(function(){
        var newValue = $(this).val();
        if(options.fromText != newValue) {
            options.fromText = newValue;
            options.from = parseFloat(newValue);
            if (isNaN(options.from) ) options.from = 0;
            calculate(false);
        }
    });

    $('#to').keyup(function(){
        var newValue = $(this).val();
        if(options.toText != newValue) {
            options.toText = newValue;
            options.to = parseFloat(newValue);
            if (isNaN(options.to) ) options.to = 0;
            calculate(true);
        }
    });

    var maskList = $.masksSort([{"mask": "+380(##)###-##-##"}], ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function(maskObj, completed) {
            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
        }
    };

    $('#your-phone').change(function() {
        $('#your-phone').inputmasks(maskOpts);
    });

    $('#your-phone').change();

    var maskListWmid = $.masksSort([{"mask": "############"}], ['#'], /[0-9]|#/, "mask");
    var maskOptsWmid = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskListWmid,
        listKey: "mask",
        onMaskChange: function(maskObj, completed) {
            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
        }
    };

    $('#your-wmid').change(function() {
        $('#your-wmid').inputmasks(maskOptsWmid);
    });

    $('#your-wmid').change();

    var maskListCard = $.masksSort([{"mask": "####-####-####-####"}], ['#'], /[0-9]|#/, "mask");
    var maskOptsCard = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskListCard,
        listKey: "mask",
        onMaskChange: function(maskObj, completed) {
            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
        }
    };

    $('#cardnumber').change(function() {
        $('#cardnumber').inputmasks(maskOptsCard);
    });

    $('#cardnumber').change();
})

Share = {
    vkontakte: function(purl, ptitle, pimg, text) {
        url  = 'http://vkontakte.ru/share.php?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image='       + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    odnoklassniki: function(purl, text) {
        url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
        url += '&st.comments=' + encodeURIComponent(text);
        url += '&st._surl='    + encodeURIComponent(purl);
        Share.popup(url);
    },
    facebook: function(purl, ptitle, pimg, text) {
        url  = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[title]='     + encodeURIComponent(ptitle);
        url += '&p[summary]='   + encodeURIComponent(text);
        url += '&p[url]='       + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);
        return false;
    },
    twitter: function(purl, ptitle) {
        url  = 'http://twitter.com/share?';
        url += 'text='      + encodeURIComponent(ptitle);
        url += '&url='      + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },
    mailru: function(purl, ptitle, pimg, text) {
        url  = 'http://connect.mail.ru/share?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&imageurl='    + encodeURIComponent(pimg);
        Share.popup(url)
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<article>
    <p class="date"><?php the_date('d M, Y'); ?></p>
    <h2><?php the_title(); ?></h2>
    <?php the_content(); ?>
    <a class="back" href="/">Вернуться назад</a>
</article>
<?php endwhile; ?>
<?php get_footer(); ?>
<?php
require('../../../wp-config.php');

$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();

$email = eto_get_option('eto_email');
$date = date("d.m.Y H:i:s");

$body = "
Дата: {$date}

WMID: {$_POST['wmid']}
Паспорт: {$_POST['passport']}
Имя: {$_POST['name']}
Фамилия: {$_POST['surname']}
Номер карты: {$_POST['cardnumber']}
Телефон: {$_POST['phone']}
Email: {$_POST['email']}
Комиссия WMT: {$_POST['wmtfee']}
Необходимо иметь: {$_POST['have']}
Комиссия обмена: {$_POST['totalfee']}
Вы получаете: {$_POST['result']}
";

echo mail($email, '[wmbaza.com.ua]', $body)?'send':'error';
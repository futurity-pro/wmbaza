<aside class="sidebar">
    <div class="block details">
        <h2>НАШИ РЕКВИЗИТЫ</h2>
        <p class="wmid"><a target="_blank" href="http://passport.webmoney.ru/asp/certview.asp?wmid=<?php echo eto_get_option('eto_wmid'); ?>"><?php echo eto_get_option('eto_wmid'); ?></a></p>
        <p class="wmz"><?php echo eto_get_option('eto_wmz'); ?></p>
        <p class="wmu"><?php echo eto_get_option('eto_wmu'); ?></p>
        <p class="wme"><?php echo eto_get_option('eto_wme'); ?></p>
        <p class="wmr"><?php echo eto_get_option('eto_wmr'); ?></p>
    </div>
    <div class="block news">
        <h2>НОВОСТИ</h2>
        <?php query_posts('post_type=post&posts_per_page=3'); ?>
        <?php while(have_posts()): the_post(); ?>
        <article>
            <p class="date"><?php the_date('d M, Y'); ?></p>
            <h3><?php the_title() ?></h3>
            <p><?php echo get_the_excerpt() ?></p>
            <a class="more" href="<?php the_permalink() ?>">Подробнее</a>
        </article>
        <?php endwhile; wp_reset_query(); ?>
        <a class="all" href="/category/news">Все новости</a>
    </div>
    <div class="block news">
        <article>
            <p><?php dynamic_sidebar('sidebar'); ?></p>
        </article>
    </div>
</aside>
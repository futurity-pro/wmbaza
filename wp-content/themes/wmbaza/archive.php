<?php get_header(); ?>
<h2>Новости</h2>
<?php while(have_posts()): the_post(); ?>
<article>
    <p class="date"><?php the_date('d M, Y'); ?></p>
    <h3><?php the_title() ?></h3>
    <p><?php echo get_the_excerpt() ?></p>
    <a class="more" href="<?php the_permalink() ?>">Подробнее</a>
</article>
<?php endwhile; ?>
<?php get_footer(); ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<title><?php wp_title( '.', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.inputmask.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bind-first-0.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.inputmask-multi.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cookie.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
    <!--[if lt IE 9]> <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> <![endif]-->
<?php wp_head(); ?>
<script>
    var options = {
        min: parseInt('<?php echo eto_get_option('eto_min'); ?>'),
        max: parseInt('<?php echo eto_get_option('eto_max'); ?>'),
        wmz_uah: parseFloat('<?php echo eto_get_option('eto_wmz-uah'); ?>'),
        wmu_uah: parseFloat('<?php echo eto_get_option('eto_wmu-uah'); ?>'),
        wme_uah: parseFloat('<?php echo eto_get_option('eto_wme-uah'); ?>'),
        wmr_uah: parseFloat('<?php echo eto_get_option('eto_wmr-uah'); ?>'),
        current: 'wmz',
        from: parseFloat('<?php echo eto_get_option('eto_from'); ?>'),
        wmz: '$',
        wmu: '₴',
        wme: '€',
        wmr: 'р.',
        delay: parseInt('<?php echo eto_get_option('eto_delay'); ?>'),
        fee: parseFloat('<?php echo eto_get_option('eto_fee'); ?>'),
        wmtfee: parseFloat('<?php echo eto_get_option('eto_wmtfee'); ?>')
    }
</script>
</head>
<body class="<?php echo is_post_type_archive('faq')?'faq':'' ?>">
<div class="wrapper">
<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/">
<span typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="http://wmbaza.com.ua/">Главная</a> › </span>
<span typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="http://wmbaza.com.ua">❶☞ Обменный сервис ☜❶‎</a> › </span>
<span typeof="v:Breadcrumb">Обменный сервис</span>
</div>
<header class="header">
    <a class="logo" href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="wmbaza.com.ua"></a>
    <section class="address">
        <p><?php echo join('</p><p>', explode("\n", eto_get_option('eto_address'))); ?></p>
    </section>

    <section class="contacts">
        <p class="email icon"><?php echo eto_get_option('eto_email'); ?></p>
        <p class="skype icon"><?php echo eto_get_option('eto_skype'); ?></p>
        <p class="phone icon"><?php echo eto_get_option('eto_phone'); ?></p>
    </section>
</header>
<nav class="nav-main">
    <?php wp_nav_menu( array('location' => 'primary' )); ?>
</nav>
<section class="<?php echo is_front_page()?'page':'content' ?>">
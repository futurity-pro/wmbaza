<?php
require_once("webmoney/src/webmoney/WMXI.php");
$wmxi = new WMXI();

$res = $wmxi->X19(
    'cash',        # тип операции
    'output',      # направление операции
    'WMZ',         # тип WM-кошелька, с/на которого произведен перевод
    10.99,         # сумма перевода
    111111111111,  # WMID пользователя
    'AA111111',    # Номер паспорта
    'Иванов',      # Фамилия пользователя
    'Иван',        # Имя пользователя
    '',            # Название банка
    '',            # Номер банковского счета
    '',            # Номер банковской карты
    '',            # Название платежной системы
    ''             # ID пользователя в платежной системе
);

print_r($res->toObject());
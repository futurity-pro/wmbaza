</section>
<section class="partners">
    <section class="webmoney">
        <a target="_blank" href="http://webmoney.ru"><img class="partner" src="<?php echo get_template_directory_uri(); ?>/img/webmoney.png"></a><br>
        <a target="_blank" href="http://passport.webmoney.ru/asp/certview.asp?wmid=<?php echo eto_get_option('eto_wmid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/atestovan.png"></a>
        <a href="http://megastock.ru/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/recieve.png"></a>
    </section>
    <section class="social">
        <img class="partner" src="<?php echo get_template_directory_uri(); ?>/img/money.png"><br>
        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo bloginfo('url') ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png"></a>
        <a target="_blank" href="https://plus.google.com/share?url=<?php echo bloginfo('url') ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/google.png"></a>
        <a target="_blank" href="http://vkontakte.ru/share.php?url=<?php echo bloginfo('url') ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/vkontakte.png"></a>
        <a target="_blank" href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=<?php echo bloginfo('url') ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/odnoklassniki.png"></a>
        <a target="_blank" href="http://connect.mail.ru/share??url=<?php echo bloginfo('url') ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/mailru.png"></a>
        <a target="_blank" href="http://twitter.com/share?url=<?php echo bloginfo('url') ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png"></a>
    </section>
</section>
<footer class="footer">
    <section class="copyright">
        <p>© <?php echo date('Y') ?> wmbaza.com.ua</p>
        <p><a href="/soglashenie">Соглашение</a></p>
    </section>
    <section class="disclaimer">
        <?php echo eto_get_option('eto_disclaimer'); ?>
    </section>
    <section class="contacts">
        E-mail: <?php echo eto_get_option('eto_email'); ?><br>
        Skype: <?php echo eto_get_option('eto_skype'); ?><br>
        Тел.: <?php echo eto_get_option('eto_phone'); ?>
    </section>
</footer>
</div>
<div id="powered">
    Сайт разработан в <a target="_blank" href="http://atsobaka.razbakov.com">ЭтСобака</a>
</div>
<?php wp_footer(); ?>
</body>
</html>
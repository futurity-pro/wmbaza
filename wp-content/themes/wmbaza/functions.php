<?php
register_nav_menu( 'primary', 'Главное Меню');

function faq_type() {
    $labels = array(
        'name'               => 'FAQ',
        'singular_name'      => 'Вопрос',
        'add_new'            => 'Новый Вопрос',
        'add_new_item'       => 'Новый Вопрос',
        'edit_item'          => 'Изменить Вопрос',
        'new_item'           => 'Новый Вопрос',
        'all_items'          => 'Все Вопросы',
        'view_item'          => 'Посмотреть Вопрос',
        'search_items'       => 'Найти Вопрос',
        'not_found'          => 'Вопросов не найдено',
        'not_found_in_trash' => 'В Корзине нет вопросов',
        'parent_item_colon'  => '',
        'menu_name'          => 'FAQ'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Раздел FAQ',
        'menu_position' => 5,
        'supports'      => array( 'title', 'editor' ),
        'public'        => true,
        'has_archive'   => true,
    );
    register_post_type( 'faq', $args );
}
add_action( 'init', 'faq_type' );

function wmz_widgets_init() {
    register_sidebar( array(
        'name' => 'Виджеты на главной',
        'id' => 'sidebar',
        'description' => 'Блок слева под новостями',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}

add_action( 'widgets_init', 'wmz_widgets_init' );

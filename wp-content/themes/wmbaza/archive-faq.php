<?php get_header(); ?>
<h2>FAQ — ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ</h2>
<?php query_posts($query_string . "&order=ASC"); ?>
<?php while ( have_posts() ) : the_post(); ?>
<article>
    <h3><?php the_title(); ?></h3>
    <div class="block">
        <?php the_content(); ?>
    </div>
</article>
<?php endwhile; ?>
<a class="back" href="/">Вернуться назад</a>
<?php get_footer(); ?>
<div class="sidebar">
    <div class="widget">
        <h3>Мы в соцсетях</h3>

        <!-- VK Widget -->
        <div id="vk_groups"></div>
        <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "300", height: "400", color1: 'FFFFFF', color2: '374D3A', color3: '374D3A'}, 55052996);
        </script>
    </div>
    <div class="widget">
        <h3>Подписаться на рассылку</h3>
        <p>Подпишитеся на рассылку и получайте из первых рук информацию о ближайших событиях, акциях, бесплатных мероприятиях и советы по самостоятельному изучению языка.</p>
        <form action="http://granitoak.us2.list-manage.com/subscribe/post?u=6b1b65a2fdb9885d429907d1d&amp;id=1c1f05bde1" method="post">
            <input name="EMAIL" type="text" placeholder="Ваш E-mail">
            <button type="submit" class="btn">Подписаться</button>
        </form>
    </div>
    <div class="widget">
        <h3>Ближайшие события</h3>

        <?php query_posts('post_type=event'); $i = 0;?>
        <?php while (have_posts()): the_post(); $i++; if ($i>3) break; ?>
        <div class="event">
            <?php
                $value = simple_fields_get_post_value(get_the_id(), 'Дата', true);
                $time = date_parse_from_format('d/m/Y', $value);
                $time = DateTime::createFromFormat('d/m/Y', $value);
                $date = $time->format('d');
                $month = $time->format('M');
                $day = $time->format('D');
            ?>
            <div class="date">
                <strong><?php echo $date ?></strong>
                <span><?php echo $month ?></span>
                <span><?php echo $day ?></span>
            </div>
            <div class="description">
                <em class="type"><?php $value = simple_fields_get_post_value(get_the_id(), 'Тип', true); echo $value; ?></em>
                <a href="<?php the_permalink() ?>" class="name"><?php the_title() ?></a>
                <span class="speaker"><?php $value = simple_fields_get_post_value(get_the_id(), 'Ведущий', true); echo $value; ?></span>
            </div>
        </div>
        <?php endwhile; wp_reset_query(); ?>
    </div>
    <div class="widget">
        <h3>Свежие записи</h3>
        <?php query_posts('post_type=post'); $i = 0;?>
        <?php while (have_posts()): the_post(); $i++; if ($i>3) break; ?>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <?php endwhile; wp_reset_query(); ?>
    </div>
    
    <?php dynamic_sidebar('sidebar'); ?>
    
    <div class="widget">
        <h3>Поиск по сайту</h3>
        <form action="<?php bloginfo('url') ?>">
            <input type="text" name="s" placeholder="Введите текст">
        </form>
    </div>
</div>
<?php
add_action( 'after_setup_theme', 'granitoak_setup' );

function granitoak_setup() {

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'left' => __( 'Left', 'granitoak' ),
        'right' => __( 'Right', 'granitoak' ),
	) );
}

function granitoak_widgets_init() {
    register_sidebar( array(
        'name' => 'Сайдбар',
        'id' => 'sidebar',
        'description' => 'Блок справа',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}

add_action( 'widgets_init', 'granitoak_widgets_init' );